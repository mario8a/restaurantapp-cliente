import React, {useState, useEffect, useContext} from 'react';
import { Link } from 'react-router-dom';
import { FirebaseContext } from '../../firebase';
import { collection, onSnapshot } from "firebase/firestore";
import Platillo from '../../ui/Platillo';

const Menu = () => {

  const [platillos, setPlatillos] = useState([]);

  const { firebaseApp } = useContext(FirebaseContext);

  useEffect(() => {

    const obtenerDatos = () => {
      // const resultado = await getDocs(collection(firebaseApp.db,'productos'));
      // console.log(resultado);
      // resultado.forEach(platillo => {
      //   console.log(platillo.data());
      // });
      onSnapshot(collection(firebaseApp.db, 'productos'), (snapshot) => {
        const platillos = snapshot.docs.map(doc => {
          return {
            id: doc.id,
            ...doc.data()
          }
        });
        // console.log(platillos);
        setPlatillos(platillos);
      });
    }
    obtenerDatos();
  }, [])
  

  return (
    <div>
      <h1 className='text-3xl font-light mb-4'>Menu</h1>
      <Link to={'/nuevo-platillo'} className="bg-blue-800 hover:bg-blue-700 inline-block mb-5 p-2 text-white uppercase font-bold rounded">
        Agregar platillo
      </Link>

      { platillos.map(platillo => (
        <Platillo
          key={platillo.id}
          platillo={platillo}
        />
      )) }

    </div>
  )
}

export default Menu