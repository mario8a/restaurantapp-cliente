import React, {useEffect, useContext, useState} from 'react'
import { collection, onSnapshot, where, query} from "firebase/firestore";
import { FirebaseContext } from '../../firebase';
import Orden from '../../ui/Orden';

const Ordenes = () => {

  const [ordenes, setOrdenes] = useState([]);
  const { firebaseApp } = useContext(FirebaseContext);

  useEffect(() => {
    const obtenerOrdenes = () => {
      const q = query(collection(firebaseApp.db, 'ordenes'),where('completado', '==', false));
      onSnapshot(q, (querySnapshot) => {
        const ordenes = querySnapshot.docs.map(doc => {
          return {
            id: doc.id,
            ...doc.data(),
          }
        });
        setOrdenes(ordenes)
      })
    };
    obtenerOrdenes();
  }, [])
  

  return (
    <>
      <h1 className='text-3xl font-light mb-4'>Ordenes</h1>
      <div className='sm:flex sm:flex-wrap mx-3'>
        {
          ordenes.map(orden => (
            <Orden
              key={orden.id}
              orden={orden}
            />
          ))
        }
      </div>
    </>
  )
}

export default Ordenes