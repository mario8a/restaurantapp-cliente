import React, { useContext, useState } from 'react';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import FirebaseContext from '../../firebase/context';
import { useNavigate } from 'react-router-dom';
import FileUploader from "react-firebase-file-uploader";
import { ref, getDownloadURL, uploadBytes } from 'firebase/storage';

const NuevoPlatillo = () => {

  //State para loas imagenes
  const [subiendo, setSubiendo] = useState(false);
  const [progreso, setProgreso] = useState(0);
  const [urlImagen, setUrlImagen] = useState('');

  //Context firebase
  const { firebaseApp } = useContext(FirebaseContext);
  const navigate = useNavigate();

  //Validacion y lerp de formulario
  const formik = useFormik({
    initialValues: {
      nombre: '',
      precio: '',
      categoria: '',
      imagen: '',
      descripcion: '',
    },
    validationSchema: Yup.object({
      nombre: Yup.string()
                 .min(3, 'Los platillos deben tener al menos 3 caracteres')
                 .required('El Nombre es obligatorio'),
      precio: Yup.number()
                 .min(1, 'Debes agregar un numero')
                 .required('El precio es obligatorio'),
      categoria: Yup.string()
                 .required('La categoria es obligatorio'),
      descripcion: Yup.string().min(10, "La descripcion debe ser las larga").required('La descripcion es obligatoria'),
    }),
    onSubmit: async (datos) => {
      try {
        datos.existencia = true;
        datos.imagen = urlImagen;
        const res = await firebaseApp.insertDocument("productos", {...datos});
        if (res.id) {
          console.log("insercción de cuerpo correcta:", res.id);
          //Redireccionar
          navigate('/menu');
        }
      } catch (error) {
        console.log(error);
      }
    }
  })

  //Imagenes files uploader
  const hadleUploadStart = () => {
    setProgreso(0);
    setSubiendo(true);
  } 
  const handleUploadError = (error) => {
    setSubiendo(false);
    console.log(error);
  } 
  const handleUploadSuccess = async (nombre) => {
    setProgreso(100);
    setSubiendo(false);

    const imageRef = ref(firebaseApp.myStorage, `${nombre}`);
    const imageRef2 = ref(firebaseApp.myStorage, `productos/${nombre}`); // Esta me da url de imagen correcta

    // const loadBytes = await uploadBytes(imageRef, nombre);
    // const url = await getDownloadURL(loadBytes);
    // console.log(url);

    uploadBytes(imageRef, nombre).then(() => {
      getDownloadURL(imageRef2).then((url) => {
        console.log(url);
        setUrlImagen(url);
      }).catch((error) => {console.log(error)});
    }).catch((error) => {console.log(error)});
    // Almacenar la URL de destino v7 / 8
    // const url = await firebaseApp.storage("productos").child(nombre).getDownloadURL();
    // console.log(url);
    // setUrlImagen(url);
  } 
  const handleProgress = (progress) => {
    setProgreso(progress);
    console.log(progress);
  } 
  
  return (
    <div>
      <h1 className='text-3xl font-light mb-4'>Agregar platillo</h1>

      <div className='flex justify-center mt-10'>
        <div className='w-full max-w-3xl'>
          <form onSubmit={formik.handleSubmit}>
            <div className='mb-4'>
              <label htmlFor="nombre" className='block text-gray-700 text-sm font-bold mb-2'>Nombre</label>
              <input 
                id='nombre'
                type="text"
                placeholder='Nombre platillo' 
                value={formik.values.nombre}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                className='shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline' />
            </div>
            {formik.touched.nombre && formik.errors.nombre ? (
              <div className='bg-red-100 border-l-4 border-red-700 p-4 mb-5' role="alert">
                <p>{formik.errors.nombre}</p>
              </div>
            ): null}
            <div className='mb-4'>
              <label htmlFor="precio" className='block text-gray-700 text-sm font-bold mb-2'>Precio</label>
              <input 
                id='precio'
                type="number"
                placeholder='$20'
                min={'0'} 
                value={formik.values.precio}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                className='shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline' />
            </div>
            {formik.touched.precio && formik.errors.precio ? (
              <div className='bg-red-100 border-l-4 border-red-700 p-4 mb-5' role="alert">
                <p>{formik.errors.precio}</p>
              </div>
            ): null}
            <div className='mb-4'>
              <label htmlFor="categoria" className='block text-gray-700 text-sm font-bold mb-2'>Categoria</label>
              <select 
                id='categoria'
                name='categoria'
                value={formik.values.categoria}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                className='shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline'>
                <option value=''>-- Seleccione --</option>
                <option value='desayuno'> Desayuno </option>
                <option value='comida'> Comida </option>
                <option value='cena'> Cena </option>
                <option value='bebidas'> Bebidas </option>
                <option value='postre'> Postres </option>
                <option value='ensaladas'> Ensaladas </option>
              </select>
              {formik.touched.categoria && formik.errors.categoria ? (
              <div className='bg-red-100 border-l-4 border-red-700 p-4 mb-5' role="alert">
                <p>{formik.errors.categoria}</p>
              </div>
            ): null}
            </div>
            <div className='mb-4'>
              <label htmlFor="imagen" className='block text-gray-700 text-sm font-bold mb-2'>Imagen</label>
              <FileUploader
                accept="image/*"
                id='imagen'
                name='imagen'
                randomizeFilename
                storageRef={firebaseApp.storage.ref('productos')}
                onUploadStart={hadleUploadStart}
                onUploadError={handleUploadError}
                onUploadSuccess={handleUploadSuccess}
                onProgress={handleProgress}
              />
             { subiendo && (
               <div className='h-12 relative w-full border'>
                 <div className='bg-green-500 absolute left-0 top-0 text-white px-2 text-sm h-12 flex items-center' style={{width: `${progreso}%`}}>
                   {progreso} %
                 </div>
               </div>
             ) }

            {urlImagen && (
              <p className='bg-green-500 text-white p-3 text-center my-5'>La imagen se subio correctamente</p>
            )}

            </div>
            <div className='mb-4'>
              <label htmlFor="descripcion" className='block text-gray-700 text-sm font-bold mb-2'>Descripcion</label>
              <textarea 
                id='descripcion'
                type="text"
                placeholder='Descripcion del platillo' 
                value={formik.values.descripcion}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                className='shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline h-40'
              ></textarea>
              {formik.touched.descripcion && formik.errors.descripcion ? (
              <div className='bg-red-100 border-l-4 border-red-700 p-4 mb-5' role="alert">
                <p>{formik.errors.descripcion}</p>
              </div>
            ): null}
            </div>

            <input
              type='submit'
              className='bg-gray-800 hover:bg-gray-900 w-full mt-5 p-2 text-white uppercase font-bold'
              value={'Agregar platillo'}
            />
          </form>
        </div>
      </div>
    </div>
  )
}

export default NuevoPlatillo