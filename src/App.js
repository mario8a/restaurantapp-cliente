import { Routes, Route } from "react-router-dom";

import firebaseApp, { FirebaseContext } from './firebase'
//Commponents
import Menu from "./components/pages/Menu";
import Ordenes from "./components/pages/Ordenes";
import NuevoPlatillo from './components/pages/NuevoPlatillo';
import Sidebar from "./ui/Sidebar";

function App() {
  return (
    <FirebaseContext.Provider value={{firebaseApp}}>
      <div className="md:flex min-h-screen">
        <Sidebar/>
        <div className="md:w-3/5 xl:w-4/5 p-6">
          <Routes>
            <Route path="/" element={<Ordenes />} />
            <Route path="/menu" element={<Menu />} />
            <Route path="/nuevo-platillo" element={<NuevoPlatillo />} />
          </Routes>
        </div>
      </div>
    </FirebaseContext.Provider>
  );
}

export default App;
