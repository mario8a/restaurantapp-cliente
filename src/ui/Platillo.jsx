import React, {useContext, useRef} from 'react'
import { FirebaseContext } from '../firebase';
import { updateDoc, doc } from 'firebase/firestore';

const Platillo = ({platillo}) => {

  // existencia ref para acceder al valor directamente
  const existenciaRef = useRef(platillo.existencia);

  //Context de firebase para cambios
  const { firebaseApp } = useContext(FirebaseContext);

  const { id ,nombre, imagen, existencia, precio, descripcion, categoria } = platillo;

  //modificar el platillo en  firebase
  const actualizarDisponibilidad = async () => {
    const existencia = (existenciaRef.current.value === "true");

    try {
      // v9
      const platilloDoc = doc(firebaseApp.db, `productos/${id}`);
      await updateDoc(platilloDoc, {existencia})
      //v7
      // firebaseApp.db.collection('productos').doc(id).update({
      //   existencia
      // });
    } catch (error) {
      console.log(error);
    }
  }

  return (
    <div className='w-full px-3 mb-4'>
      <div className='p-5 shadow-md bg-white'>
        <div className='lg:flex'>
          <div className='lg:w-5/12 xl:w-3/12'>
            <img src={imagen} alt="Imagen platillo" />
            <div className='sm:flex sm:-mx-2'>
              <label htmlFor="" className='block mt-5 sm:w-2/4'>
                <span className='block text-gray-800 mb-2'>Existencia</span>

                <select 
                  name="" 
                  id="" 
                  className='shadow appearance-none border rounded w-full py-2 px-3 leading-tight focus:outline-none'
                  value={existencia}
                  ref={existenciaRef}
                  onChange={() => actualizarDisponibilidad()}
                >
                  <option value="true">Disponible</option>
                  <option value="false">No Disponible</option>
                </select>
              </label>
            </div>
          </div>
          <div className='lg:w-7/12 xl:w-9/12'>
            <p className='font-bold text-2xl text-yellow-600 mb-4'>{nombre}</p>
            <p className='text-gray-700 font-bold'> Categoria: {''}
              <span className='text-gray-700 font-bold'>{categoria.toUpperCase()}</span>
            </p>
            <p className='text-gray-600 mb-4'>{descripcion}</p>
            <p className='text-gray-600 mb-4'>Precio: {''}
              <span className='text-gray-700 font-bold'>${precio}</span>
            </p>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Platillo